
/*
  Temperature Sensor

  Reads the current temperature using a DHT11 sensor and sends it in CSV format over Serial.

Author: Kevin Bishop
Date: 2020-02-23
*/


#define DHT11_PIN 7
#include <dht.h>

//DHT is the class that reads from DHT11 sensor
dht DHT;
volatile int observationNum = 0; 
int interruptCount = 0;
volatile bool readTempFlag = false;


//Unit conversion for Americans
int celsiusToFahrenheit(int temp){
  return 1.8*temp+32;
}

//This ISR sets a flag to the temperature every 10 times the Timer1 interrupt is triggered
ISR(TIMER1_COMPA_vect){
  interruptCount++;
  //Only measure temperature every 10 seconds
  if (interruptCount >= 10){
    interruptCount = 0;
    readTempFlag = true;
    observationNum++;
  }
}

//NOTE: the following arduino Timer1 set up code was found here: https://www.instructables.com/id/Arduino-Timer-Interrupts/
void setup(){
  cli();   //disable interrupts
  Serial.begin(9600);
  //set timer1 interrupt at 1Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 15624;// = (16*10^6) / (1*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS10 and CS12 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  sei(); //enable interrupts
}

//When the readTempFlag is set, read and print out the temperature, then reset the flag
void loop()
{
  int chk = DHT.read11(DHT11_PIN);
  if ((chk == -1) & (readTempFlag)){
    Serial.print(observationNum);
    Serial.print(",");
    Serial.println(celsiusToFahrenheit(DHT.temperature));
    readTempFlag = false;
  }
}
